package servicetests;

import administration.DTUMerchantRepository;
import administration.Entities.DTUMerchant;
import administration.Entities.Merchant;
import administration.MerchantRepository;
import entities.MerchantDTO;
import entities.MerchantValidationDTO;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import listeners.ValidateMerchantListener;
import messaging.MessageSender;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ValidatingMerchantsSteps {

    Merchant merchant;
    MerchantRepository repository = DTUMerchantRepository.getInstance();
    MessageSender sender = mock(MessageSender.class);
    ValidateMerchantListener listener = new ValidateMerchantListener(repository, sender);


    @Given("a merchant is created")
    public void aMerchantIsCreated() {
        merchant = new DTUMerchant("John", "Doe", "13Merchant");
        repository.createMerchant(merchant);
    }

    @When("the service receives a {string} event")
    public void theServiceReceivesAEvent(String arg0) {
        MerchantDTO merchantDTO = new MerchantDTO("transaction-id", "13Merchant");
        listener.receive(merchantDTO);
    }

    @Then("the service validates the merchant")
    public void theServiceValidatesTheMerchant() {
        assertEquals(merchant, repository.getMerchantByCpr(merchant.getCpr()));
    }

    @And("the {string} event is broadcast")
    public void theEventIsBroadcast(String arg0) {
        verify(sender, times(1)).sendMessage(any(MerchantValidationDTO.class),eq("merchant.validate.transaction-id"), eq("payment"));
    }
}
