package unittests.administration;

import administration.DTUCustomerRepository;
import administration.DTUMerchantRepository;
import administration.Entities.DTUMerchant;
import administration.Entities.Merchant;
import administration.MerchantRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Similar to DTUMerchantRepositoryTest
 */
public class DTUMerchantRepositoryTest {

    private MerchantRepository repository;

    @Before
    public void setup() {
        repository = DTUMerchantRepository.getInstance();
    }

    @After
    public void cleanup() {
        DTUMerchantRepository.clean();
    }

    @Test
    public void createMerchant_createdMerchant_isRecognized() {
        String cpr = "012345-6789";
        Merchant merchant = new DTUMerchant("first", "last", cpr);
        repository.createMerchant(merchant);
        Merchant storedMerchant = repository.getMerchantByCpr(cpr);
        assertEquals(merchant.getFirstName(), storedMerchant.getFirstName());
        assertEquals(merchant.getLastName(), storedMerchant.getLastName());
        assertEquals(merchant.getCpr(), storedMerchant.getCpr());
    }

    @Test
    public void deleteMerchant_deletedMerchant_isNotRecognized() {
        String cpr = "012345-6789";
        Merchant merchant = new DTUMerchant("first", "last", cpr);
        repository.createMerchant(merchant);
        assertTrue(repository.deleteMerchant(cpr));
        assertNull(repository.getMerchantByCpr(cpr));
    }

    @Test
    public void deleteMerchant_unknownMerchant_cannotBeDeleted() {
        String cpr = "012345-6789";
        Merchant merchant = new DTUMerchant("first", "last", cpr);
        assertFalse(repository.deleteMerchant(merchant.getCpr()));
    }

    @Test
    public void deleteMerchant_differentMerchantInstance_isDeleted() {
        String cpr = "012345-6789";
        Merchant merchant = new DTUMerchant("first", "last", cpr);
        repository.createMerchant(merchant);
        Merchant merchantInstance2 = new DTUMerchant(merchant.getFirstName(),
                merchant.getLastName(), merchant.getCpr());
        assertTrue(repository.deleteMerchant(merchantInstance2.getCpr()));
        assertNull(repository.getMerchantByCpr(cpr));
    }

}
