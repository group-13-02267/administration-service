package unittests.administration;

import administration.CustomerAdministrationService;
import administration.CustomerRepository;
import administration.DTUCustomerAdministrationService;
import administration.Entities.Customer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DTUCustomerAdministrationServiceTest {

    private CustomerAdministrationService service;
    private CustomerRepository repository;
    private Map<String, Customer> repositoryStorage;


    @Before
    public void setup() {
        repositoryStorage = new HashMap<>();
        repository = mock(CustomerRepository.class);

        // Mocked repository methods that makes it behave like a "proper" repository
        when(repository.createCustomer(any(Customer.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Customer customer = invocation.getArgument(0);
                repositoryStorage.put(customer.getCpr(), customer);
                return customer;
            }
        });
        when(repository.getCustomerByCpr(any(String.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                return repositoryStorage.get((String) invocation.getArgument(0));
            }
        });
        when(repository.deleteCustomer(any(String.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Customer customer = repositoryStorage.get(invocation.getArgument(0));
                if (customer == null) {
                    return false;
                }
                return repositoryStorage.remove(customer.getCpr(), customer);
            }
        });

        service = new DTUCustomerAdministrationService(repository);
    }

    @Test
    public void createCustomer_validCustomer_canBeCreated() {
        String firstName = "first";
        String lastName = "last";
        String cpr = "012345-6789";
        DTUPayResponse<Customer> response = service.createNewCustomer(firstName, lastName, cpr);
        assertNotNull(response);
        assertTrue(response.success());
        Customer createdCustomer = response.getResponse();
        assertEquals(firstName, createdCustomer.getFirstName());
        assertEquals(lastName, createdCustomer.getLastName());
        assertEquals(cpr, createdCustomer.getCpr());
    }

    @Test
    public void createNewCustomer_invalidCustomer_cannotBeCreated() {
        String firstName = "first";
        String lastName = "last";
        String cpr = "012345-6789";

        DTUPayResponse<Customer> response = service.createNewCustomer(null, null, null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createNewCustomer(firstName, lastName, null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createNewCustomer(firstName, null, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createNewCustomer(null, lastName, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createNewCustomer(null, null, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());
    }

    @Test
    public void createNewCustomer_customersWithIdenticalCpr_cannotBeCreated() {
        String cpr = "012345-6789";
        // Create two customers with identical cpr
        DTUPayResponse<Customer> response1 = service.createNewCustomer("first1", "last1", cpr);
        DTUPayResponse<Customer> response2 = service.createNewCustomer("first2", "last2", cpr);
        assertTrue(response1.success());
        assertFalse(response2.success());
    }

    @Test
    public void createNewCustomer_customersWithSameNamesDifferentCpr_canBeCreated() {
        // Create two customers with same names but different CPR
        DTUPayResponse<Customer> response1 = service.createNewCustomer("first", "last", "111111-1111");
        DTUPayResponse<Customer> response2 = service.createNewCustomer("first", "last", "222222-2222");
        assertTrue(response1.success());
        assertTrue(response2.success());
    }

    @Test
    public void deleteCustomer_existinngCustomer_successOnDelete() {
        String cpr = "012345-6789";
        service.createNewCustomer("first", "last", cpr);
        DTUPayEmptyResponse response = service.deleteCustomer(cpr);
        assertTrue(response.success());
        assertNull(response.getErrorMessage());
    }

    @Test
    public void deleteCustomer_unknownCustomer_errorOnDelete() {
        DTUPayEmptyResponse response = service.deleteCustomer("012345-6789");
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
    }

    @Test
    public void deleteCustomer_noCpr_errorOnDelete() {
        DTUPayEmptyResponse response = service.deleteCustomer(null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
    }
}
