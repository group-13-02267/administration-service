package unittests.administration.Entities;

import administration.Entities.DTUCustomer;
import administration.Entities.DTUUser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DTUUserTest {

    @Test
    public void newDTUUser_whenInstantiatingNewUser_shouldInstantiateObject() {
        DTUUser user = new DTUCustomer("first", "last", "cpr");
        assertEquals("first", user.getFirstName());
        assertEquals("last", user.getLastName());
        assertEquals("cpr", user.getCpr());
    }

    @Test
    public void isValid_checkingIsValid_shouldBeInvalid() {
        DTUUser user1 = new DTUCustomer(null, "last", "cpr");
        DTUUser user2 = new DTUCustomer("first", null, "cpr");
        DTUUser user3 = new DTUCustomer("first", "last", null);
        DTUUser user4 = new DTUCustomer(null, null, null);
        assertFalse(user1.isValid());
        assertFalse(user2.isValid());
        assertFalse(user3.isValid());
        assertFalse(user4.isValid());
    }

}
