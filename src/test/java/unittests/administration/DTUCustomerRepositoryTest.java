package unittests.administration;

import administration.CustomerRepository;
import administration.DTUCustomerRepository;
import administration.Entities.Customer;
import administration.Entities.DTUCustomer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DTUCustomerRepositoryTest {

    private CustomerRepository repository;

    @Before
    public void setup() {
        repository = DTUCustomerRepository.getInstance();
    }

    @After
    public void cleanup() {
        DTUCustomerRepository.clean();
    }

    @Test
    public void createCustomer_createdCustomer_isRecognized() {
        String cpr = "012345-6789";
        Customer customer = new DTUCustomer("first", "last", cpr);
        repository.createCustomer(customer);
        Customer storedCustomer = repository.getCustomerByCpr(cpr);
        assertEquals(customer.getFirstName(), storedCustomer.getFirstName());
        assertEquals(customer.getLastName(), storedCustomer.getLastName());
        assertEquals(customer.getCpr(), storedCustomer.getCpr());
    }

    @Test
    public void deleteCustomer_deletedCustomer_isNotRecognized() {
        String cpr = "012345-6789";
        Customer customer = new DTUCustomer("first", "last", cpr);
        repository.createCustomer(customer);
        assertTrue(repository.deleteCustomer(customer.getCpr()));
        assertNull(repository.getCustomerByCpr(cpr));
    }

    @Test
    public void deleteCustomer_unknownCustomer_cannotBeDeleted() {
        String cpr = "012345-6789";
        Customer customer = new DTUCustomer("first", "last", cpr);
        assertFalse(repository.deleteCustomer(customer.getCpr()));
    }

    @Test
    public void deleteCustomer_differentCustomerInstance_isDeleted() {
        String cpr = "012345-6789";
        Customer customer = new DTUCustomer("first", "last", cpr);
        repository.createCustomer(customer);
        Customer customerInstance2 = new DTUCustomer(customer.getFirstName(),
                customer.getLastName(), customer.getCpr());
        assertTrue(repository.deleteCustomer(customerInstance2.getCpr()));
        assertNull(repository.getCustomerByCpr(cpr));
    }

}
