package unittests.administration;

import administration.DTUMerchantAdministrationService;
import administration.Entities.Merchant;
import administration.MerchantAdministrationService;
import administration.MerchantRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Similar to DTUMerchantAdministrationTest
 */
public class DTUMerchantAdministrationServiceTest {

    private MerchantAdministrationService service;
    private MerchantRepository repository;
    private Map<String, Merchant> repositoryStorage;

    @Before
    public void setup() {
        repositoryStorage = new HashMap<>();

        repository = mock(MerchantRepository.class);
        // Mocked repository methods that makes it behave like a "proper" repository
        when(repository.createMerchant(any(Merchant.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Merchant merchant = invocation.getArgument(0);
                repositoryStorage.put(merchant.getCpr(), merchant);
                return merchant;
            }
        });
        when(repository.getMerchantByCpr(any(String.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                return repositoryStorage.get((String) invocation.getArgument(0));
            }
        });
        when(repository.deleteMerchant(any(String.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Merchant merchant = repositoryStorage.get(invocation.getArgument(0));
                if (merchant == null) {
                    return false;
                }
                return repositoryStorage.remove(merchant.getCpr(), merchant);
            }
        });

        service = new DTUMerchantAdministrationService(repository);
    }

    @Test
    public void createMerchant_validMerchant_canBeCreated() {
        String firstName = "first";
        String lastName = "last";
        String cpr = "012345-6789";
        DTUPayResponse<Merchant> response = service.createMerchant(firstName, lastName, cpr);
        assertNotNull(response);
        assertTrue(response.success());
        Merchant createdMerchant = response.getResponse();
        assertEquals(firstName, createdMerchant.getFirstName());
        assertEquals(lastName, createdMerchant.getLastName());
        assertEquals(cpr, createdMerchant.getCpr());
    }

    @Test
    public void createNewMerchant_invalidMerchant_cannotBeCreated() {
        String firstName = "first";
        String lastName = "last";
        String cpr = "012345-6789";

        DTUPayResponse<Merchant> response = service.createMerchant(null, null, null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createMerchant(firstName, lastName, null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createMerchant(firstName, null, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createMerchant(null, lastName, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());

        response = service.createMerchant(null, null, cpr);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
        assertNull(response.getResponse());
        assertNotNull(response.getErrorMessage().getMessage());
    }

    @Test
    public void createNewMerchant_merchantsWithIdenticalCpr_cannotBeCreated() {
        String cpr = "012345-6789";
        // Create two merchants with identical cpr
        DTUPayResponse<Merchant> response1 = service.createMerchant("first1", "last1", cpr);
        DTUPayResponse<Merchant> response2 = service.createMerchant("first2", "last2", cpr);
        assertTrue(response1.success());
        assertFalse(response2.success());
    }

    @Test
    public void createNewMerchant_merchantsWithSameNamesDifferentCpr_canBeCreated() {
        // Create two merchants with same names but different CPR
        DTUPayResponse<Merchant> response1 = service.createMerchant("first", "last", "111111-1111");
        DTUPayResponse<Merchant> response2 = service.createMerchant("first", "last", "222222-2222");
        assertTrue(response1.success());
        assertTrue(response2.success());
    }

    @Test
    public void deleteMerchant_existingMerchant_successOnDelete() {
        String cpr = "012345-6789";
        service.createMerchant("first", "last", cpr);
        DTUPayEmptyResponse response = service.deleteMerchant(cpr);
        assertTrue(response.success());
        assertNull(response.getErrorMessage());
    }

    @Test
    public void deleteMerchant_unknownMerchant_errorOnDelete() {
        DTUPayEmptyResponse response = service.deleteMerchant("012345-6789");
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
    }

    @Test
    public void deleteMerchant_noCpr_errorOnDelete() {
        DTUPayEmptyResponse response = service.deleteMerchant(null);
        assertFalse(response.success());
        assertNotNull(response.getErrorMessage());
    }
}
