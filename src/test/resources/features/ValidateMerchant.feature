Feature: Validating merchants
  Scenario: A valid merchant is validated
    Given a merchant is created
    When the service receives a "validate.merchant" event
    Then the service validates the merchant
    And the "merchant.validated.id" event is broadcast


