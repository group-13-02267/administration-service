package entities;

/**
 * Data Transfer Object used for creating customers
 * @author Roberts
 *
 */
public class CustomerCreationDTO {
	public String instanceId;
	 public String cpr;
	  public String firstName;
	  public String lastName;
}
