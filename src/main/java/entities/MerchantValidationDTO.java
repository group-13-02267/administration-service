package entities;

/**
 * Data Transfer Object used as a response for the merchant validation
 * @author
 *
 */
public class MerchantValidationDTO {
    public String id;
    public boolean status;

    /**
     * Constructor for the Class
     */
    public MerchantValidationDTO() {

    }

    /**
     * Alternate constructor for the Class
     * @param id - the id of the validation request
     * @param status - true if merchant has been validated, false otherwise
     */
    public MerchantValidationDTO(String id, boolean status) {
        this.id = id;
        this.status = status;
    }
}
