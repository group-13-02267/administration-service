package entities;

/**
 * Data Transfer Object used for Responses
 * @author robis
 *
 */
public class ResponseDTO {
	/**
	 * Constructor for the Class
	 * @param success Indicates whether the desired action was performed successfully
	 */

    public ResponseDTO(boolean success) {
        this.success = success;
    }
    /**
     * Alternate constructor for the Class
     */
    public ResponseDTO() {
    }

    public boolean success;
}
