package entities;

/**
 * Data Transfer Object used for creating merchants
 * @author
 *
 */
public class MerchantCreationDTO {
	public String instanceId;
	  public String cpr;
	  public String firstName;
	  public String lastName;
}
