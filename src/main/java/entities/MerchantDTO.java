package entities;

/**
 * Data Transfer Object for Merchant used when receiving a request from the message queue
 * @author 
 *
 */
public class MerchantDTO {
    public String id;
    public String merchantId;

    /**
     * Constructor for the Merchant
     */
    public MerchantDTO() {

    }

    /**
     * Alternate constructor for the Merchant
     * @param id - the id of the validation request
     * @param merchantId - the id of the merchant that needs to be validated. Currently id equals the CPR number of the merchant
     */
    public MerchantDTO (String id, String merchantId) {
        this.id = id;
        this.merchantId = merchantId;
    }
}
