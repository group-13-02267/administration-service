package messaging;

import core.AsyncSender;
import core.RabbitSender;

public class RabbitMqMessageSenderService implements MessageSender {
    @Override
    public void sendMessage(Object content, String route, String exchange) {
        AsyncSender<Object> eventSender = new RabbitSender<>(exchange, route);
        eventSender.send(content);
    }
}
