package messaging;

public interface MessageSender {
    void sendMessage(Object content, String route, String exchange);
}
