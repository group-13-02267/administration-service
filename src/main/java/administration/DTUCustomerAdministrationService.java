package administration;

import administration.Entities.Customer;
import administration.Entities.DTUCustomer;
import responses.*;

/**
 * The implementation class of the Customer Administration Service
 * @author 
 *
 */

public class DTUCustomerAdministrationService implements CustomerAdministrationService {

    private final CustomerRepository customerRepository;
    
    /**
     * The constructor of the class
     * @param userRepository A customer repository
     */
    public DTUCustomerAdministrationService(CustomerRepository userRepository) {
        this.customerRepository = userRepository;
    }

    /**
     * 
     */
    @Override
    public DTUPayEmptyResponse deleteCustomer(String cpr) {
        if (cpr == null)
            return new DTUPayEmptyResponseImpl(createErrorMessage("cpr is invalid"));

        boolean success = customerRepository.deleteCustomer(cpr);
        if (!success) {
            return new DTUPayEmptyResponseImpl(createErrorMessage("customer does not exist"));
        }

        return new DTUPayEmptyResponseImpl();
    }
    
    /**
     * 
     */
    @Override
    public DTUPayResponse<Customer> createNewCustomer(String firstName, String lastName, String cpr) {
        Customer customer = new DTUCustomer(firstName, lastName, cpr);
        if (!customer.isValid()) {
            return new DTUPayResponseImpl<Customer>(createErrorMessage("invalid parameters"));
        }
        if (customerRepository.getCustomerByCpr(cpr) != null) {
            return new DTUPayResponseImpl<Customer>(createErrorMessage(
                    "customer with provided CPR already exists"));
        }
        Customer persistedCustomer = customerRepository.createCustomer(customer);
        if (persistedCustomer == null) {
            return new DTUPayResponseImpl<Customer>(createErrorMessage(
                    "internal error: customer could not be created"));
        }
        return new DTUPayResponseImpl<Customer>(customer);
    }
    
    /**
     * Helper method used for creating a new DTUPayErrorMessage
     * @param errorMsg input message
     * @return DTUPayErrorMessage 
     */
    private ErrorMessage createErrorMessage(String errorMsg) {
        return new DTUPayErrorMessage(errorMsg);
    }
}
