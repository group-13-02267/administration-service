package administration;

import administration.Entities.Merchant;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation class of the MerchantRepository
 * @author  
 *
 */
public class DTUMerchantRepository implements MerchantRepository {
    private List<Merchant> merchants = new ArrayList<>();

    private static DTUMerchantRepository instance;

    /**
     * The constructor of the class
     */
    private DTUMerchantRepository() {

    }

    /**
     * The method that creates a new instance of the class
     * @return DTUMerchantRepository
     */
    public static MerchantRepository getInstance() {
        if (instance == null) {
            instance = new DTUMerchantRepository();
        }
        return instance;
    }

    /**
     * 
     */
    @Override
    public Merchant createMerchant(Merchant merchant) {
        if (exists(merchant)) {
            throw new IllegalArgumentException("Merchant already exists");
        }
        merchants.add(merchant);
        return merchant;
    }

    /**
     * 
     */
    @Override
    public boolean deleteMerchant(String merchantId) {
        return merchants.removeIf(x -> x.getCpr().equals(merchantId));
    }

    /**
     * 
     */
    @Override
    public Merchant getMerchantByCpr(String cpr) {
        return merchants.stream().filter(u -> u.getCpr().equals(cpr)).findFirst().orElse(null);
    }

    /**
     * Helper method used to verify if a merchant already exists in the merchants ArrayList
     * @param merchant A Merchant object
     * @return True if a Merchant object with the same CPR already exists, false otherwise
     */
    private boolean exists(Merchant merchant) {
        return merchants
                .stream()
                .anyMatch(mer -> mer.getCpr().equals(merchant.getCpr()));
    }

    /**
     * Delete all entries in the merchants ArrayList
     */
    public static void clean() {
        instance.merchants = new ArrayList<>();
    }
}
