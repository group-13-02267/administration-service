package administration;

import administration.Entities.Merchant;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

/**
 * This is an interface for the merchant administration service.
 *
 * @author 
 *
 */
public interface MerchantAdministrationService {
	/**
	 * Used for creating a new merchant
	 * @param firstName The first name of the merchant
	 * @param lastName The last name of the merchant
	 * @param cpr The CPR number of the merchant
	 * @return DTUPayResponse object
	 */
    DTUPayResponse<Merchant> createMerchant(String firstName, String lastName, String cpr);

    /**
     * Used for deleting an existing merchant
     * @param merchantId The id of the merchant which should be deleted. Currently id equals the CPR number of the merchant.
     * @return DTUPayResponse object
     */
    DTUPayEmptyResponse deleteMerchant(String merchantId);
}
