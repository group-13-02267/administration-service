package administration;

import administration.Entities.Customer;

import java.util.ArrayList;
import java.util.List;
/**
 * The implementation class of the CustomerRepository
 * @author  
 *
 */
public class DTUCustomerRepository implements CustomerRepository {
    private List<Customer> customers = new ArrayList<>();

    private static DTUCustomerRepository instance;

    /**
     * The constructor of the class
     */
    public DTUCustomerRepository() {

    }

    /**
     * The method that creates a new instance of the class
     * @return DTUCustomerRepository
     */
    public static CustomerRepository getInstance() {
        if (instance == null) {
            instance = new DTUCustomerRepository();
        }
        return instance;
    }

    /**
     * 
     */
    @Override
    public Customer createCustomer(Customer customer) {
        customers.add(customer);
        return customer;
    }

    /**
     * 
     */
    @Override
    public boolean deleteCustomer(String customerId) {
        return customers.removeIf(x -> x.getCpr() == customerId);
    }

    /**
     * 
     */
    @Override
    public Customer getCustomerByCpr(String cpr) {
        return customers.stream().filter(u -> u.getCpr().equals(cpr)).findFirst().orElse(null);
    }

    /**
     * Deletes all entries in the customers ArrayList
     */
    public static void clean() {
        instance.customers = new ArrayList<>();
    }
}
