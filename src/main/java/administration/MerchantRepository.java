package administration;

import administration.Entities.Merchant;
/**
 * This is a repository used by the Merchant Administration Service
 * @author 
 *
 */

public interface MerchantRepository {
    /**
     * Creates a new merchant by persisting it in the system
     * @param merchant A Merchant object
     * @return merchant
     */
    Merchant createMerchant(Merchant merchant);

    /**
     * deletes a merchant from the system
     * @param merchantId The id of the merchant that should be deleted. Currently id equals the CPR number of the customer
     * @return True if successful, false otherwise
     */
    boolean deleteMerchant(String merchantId);

    /**
     * gets merchant based on the CPR number
     * @param cpr The CPR number of the merchant
     * @return The found merchant if any else it returns null
     */
    Merchant getMerchantByCpr(String cpr);
}
