package administration;

import administration.Entities.Customer;
/**
 * This is a repository used by the Customer Administration Service
 * @author 
 *
 */
public interface CustomerRepository {
/**
 * Creates a new user by persisting it in the system
 * @param customer A Customer Object
 * @return customer
 */
    Customer createCustomer(Customer customer);

/**
 * Deletes an user from the system
 * @param customerId The id of the customer that should be deleted. Currently id equals the CPR number of the customer
 * @return True if successful, false otherwise
 */
    boolean deleteCustomer(String customerId);

/**
 * Gets user based on CPR number
 * @param cpr The CPR number of the customer
 * @return The found Customer if any else it returns null
 */
    Customer getCustomerByCpr(String cpr);
}
