package administration.Entities;

/**
 * Implementation class of the Customer
 * @author 
 *
 */
public class DTUCustomer extends DTUUser implements Customer {
	/**
	 * Constructor for the Customer
	 * @param firstName First Name of the customer
	 * @param lastName Last Name of the customer
	 * @param cpr CPR number of the customer
	 */
    public DTUCustomer(String firstName, String lastName, String cpr) {
        initialize(firstName, lastName, cpr);
    }
}
