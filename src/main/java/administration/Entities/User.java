package administration.Entities;

/**
 * Interface for the User entity
 * @author 
 *
 */
public interface User {
	/**
	 *  Method for getting the CPR of the user
	 * @return CPR number of the user
	 */
    String getCpr();

    /**
     * Method for getting the First Name of the user
     * @return First name of the user
     */
    String getFirstName();

    /**
     * Method for getting the Last Name of the user
     * @return Last name of the user
     */
    String getLastName();

    /**
     * Function that checks if the User is valid
     * @return False if firstName, lastName or cpr are null, True otherwise
     */
    boolean isValid();
}
