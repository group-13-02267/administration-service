package administration.Entities;

public abstract class DTUUser implements User {
    private String cpr;
    private String firstName;
    private String lastName;

    /**
     * Method that is used to set the private variables
     * @param firstName First name of the User
     * @param lastName Last name of the User
     * @param cpr CPR number of the User
     */
    protected void initialize(String firstName, String lastName, String cpr) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * 
     */
    public String getCpr() {
        return cpr;
    }
    /**
     * 
     */
    public boolean isValid() {
        return cpr != null && firstName != null && lastName != null;
    }
    /**
     * 
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * 
     */
    public String getLastName() {
        return lastName;
    }
}
