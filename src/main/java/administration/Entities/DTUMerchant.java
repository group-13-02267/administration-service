package administration.Entities;

/**
 * Implementation class of the Merchant entity
 * @author 
 *
 */
public class DTUMerchant extends DTUUser implements Merchant {
	/**
	 * Constructor for the Merchant
	 * @param firstName First Name of the merchant
	 * @param lastName Last Name of the merchant
	 * @param cpr CPR number of the merchant
	 */
    public DTUMerchant(String firstName, String lastName, String cpr) {
        initialize(firstName, lastName, cpr);
    }
}
