package administration;

import administration.Entities.Customer;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

/**
 * This is an interface for the customer administration service.
 *
 * @author 
 *
 */
public interface CustomerAdministrationService {
	/**
	 * Used for creating a new customer
	 * @param firstName The first name of the customer
	 * @param lastName The last name of the customer
	 * @param cpr The CPR number of the customer
	 * @return DTUPayResponse object
	 */
    DTUPayResponse<Customer> createNewCustomer(String firstName, String lastName, String cpr);

    /**
     * Used for deleting an existing customer
     * @param customerId The id of the customer which should be deleted. Currently id equals the CPR number of the customer.
     * @return DTUPayResponse object
     */
    DTUPayEmptyResponse deleteCustomer(String customerId);
}
