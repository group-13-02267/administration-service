package administration;

import administration.Entities.DTUMerchant;
import administration.Entities.Merchant;
import responses.*;

/**
 * The implementation class of the Merchant Administration Service
 * @author 
 *
 */
public class DTUMerchantAdministrationService implements MerchantAdministrationService {
    private final MerchantRepository merchantRepository;

    /**
     * The constructor of the class
     * @param merchantRepository A merchant repository
     */
    public DTUMerchantAdministrationService(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    /**
     * 
     */
    @Override
    public DTUPayResponse<Merchant> createMerchant(String firstName, String lastName, String cpr) {
        Merchant merchant = new DTUMerchant(firstName, lastName, cpr);

        if (!merchant.isValid()) {
            return new DTUPayResponseImpl<Merchant>(createErrorMessage("invalid parameters"));
        }

        if (merchantRepository.getMerchantByCpr(cpr) != null) {
            return new DTUPayResponseImpl<>(createErrorMessage(
                    "customer with provided CPR already exists"));
        }
        merchant = merchantRepository.createMerchant(merchant);
        return new DTUPayResponseImpl<>(merchant);
    }

    /**
     * 
     */
    @Override
    public DTUPayEmptyResponse deleteMerchant(String cpr) {
        if (cpr == null)
            return new DTUPayEmptyResponseImpl(createErrorMessage("cpr is invalid"));

        boolean success = merchantRepository.deleteMerchant(cpr);
        if (success) {
            return new DTUPayEmptyResponseImpl();
        }
        return new DTUPayEmptyResponseImpl(new DTUPayErrorMessage("could not create merchant with id: " + cpr));
    }

    /**
     * Helper method used for creating a new DTUPayErrorMessage
     * @param errorMsg input message
     * @return DTUPayErrorMessage 
     */
    private ErrorMessage createErrorMessage(String errorMsg) {
        return new DTUPayErrorMessage(errorMsg);
    }
}
