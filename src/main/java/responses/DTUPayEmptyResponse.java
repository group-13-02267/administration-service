package responses;

public interface DTUPayEmptyResponse {
    boolean success();

    ErrorMessage getErrorMessage();
}
