package responses;

public interface DTUPayResponse<T> extends DTUPayEmptyResponse {
    T getResponse();
}
