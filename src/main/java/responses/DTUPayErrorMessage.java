package responses;

public class DTUPayErrorMessage implements ErrorMessage {
    private String message;

    public DTUPayErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
