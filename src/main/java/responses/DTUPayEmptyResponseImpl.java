package responses;

public class DTUPayEmptyResponseImpl implements DTUPayEmptyResponse {

    private ErrorMessage errorMessage;

    public DTUPayEmptyResponseImpl() {
    }

    public DTUPayEmptyResponseImpl(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean success() {
        return errorMessage == null;
    }

    @Override
    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
