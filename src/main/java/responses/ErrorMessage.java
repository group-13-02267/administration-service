package responses;

public interface ErrorMessage {
    String getMessage();
}
