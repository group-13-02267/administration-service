package responses;

public class DTUPayResponseImpl<T> extends DTUPayEmptyResponseImpl implements DTUPayResponse<T> {

    private T response;
    private ErrorMessage errorMessage;

    public DTUPayResponseImpl(T response) {
        this.response = response;
    }

    public DTUPayResponseImpl(ErrorMessage errorMessage) {
        super(errorMessage);
    }

    @Override
    public T getResponse() {
        return response;
    }
}
