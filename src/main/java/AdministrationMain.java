import administration.CustomerAdministrationService;
import administration.CustomerRepository;
import administration.DTUCustomerAdministrationService;
import administration.DTUCustomerRepository;
import administration.DTUMerchantAdministrationService;
import administration.DTUMerchantRepository;
import administration.MerchantAdministrationService;
import administration.MerchantRepository;
import core.AsyncException;
import core.Microservice;
import listeners.CreateCustomerListener;
import listeners.CreateMerchantListener;
import listeners.ValidateMerchantListener;
import messaging.MessageSender;
import messaging.RabbitMqMessageSenderService;

/**
 *This is the main class of the Administration microservice,
 * which is responsible for creating Customers and Merchants 
 * @author 
 *
 */

public class AdministrationMain extends Microservice {
	/**
	 * This is the main method which instantiates a new instance of the class, and calls the startup method 
	 * @param args
	 * @throws Exception
	 */

	
    public static void main(String[] args) throws Exception {
        new AdministrationMain().startup();
    }

    /**
	 * This is the startup method, which is used to create listeners, their dependencies, and start listening for a message
	 * @throws AsyncException
	 */
    @Override
    public void startup() throws AsyncException {
    	
    	MessageSender sender = new RabbitMqMessageSenderService();
    	MerchantRepository merchantRepository=DTUMerchantRepository.getInstance();
        CustomerRepository customerRepository=new DTUCustomerRepository();

        MerchantAdministrationService merchantAdministrationService=new DTUMerchantAdministrationService(merchantRepository);
        CustomerAdministrationService customerAdministrationService=new DTUCustomerAdministrationService(customerRepository);
        
        ValidateMerchantListener validateMerchantListener = new ValidateMerchantListener(merchantRepository, sender);
        validateMerchantListener.listen();
        CreateMerchantListener createMerchantListener=new CreateMerchantListener(merchantAdministrationService);
        createMerchantListener.listen();
        CreateCustomerListener createCustomerListener=new CreateCustomerListener(customerAdministrationService);
        createCustomerListener.listen();
    }
}
