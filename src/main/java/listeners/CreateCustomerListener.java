package listeners;

import com.google.gson.Gson;

import administration.CustomerAdministrationService;
import administration.Entities.Customer;
import core.AsyncSender;
import core.AsyncService;
import core.RabbitSender;
import entities.CustomerCreationDTO;
import entities.ResponseDTO;
import responses.DTUPayResponse;

/**
 * This class is responsible for listening to messages regarding the customer creation, 
 * as well as performing the correct action and sending a response.
 * This class extends the functionality of the AsyncService
 * @author Roberts
 *
 */
public class CreateCustomerListener extends AsyncService<CustomerCreationDTO> {

	private final static String exchange = "administration";
	private final CustomerAdministrationService customerAdministrationService;
	
	/**
	 * Constructor for the Class
	 * @param customerAdministrationService A service that will be called once a message has been received
	 */
    public CreateCustomerListener(CustomerAdministrationService customerAdministrationService) {
        super(exchange, "administration.customer.create.*");
        this.customerAdministrationService=customerAdministrationService;
    }

    /**
     * Handle incoming payload. Converts the payload to a CustomerCreationDTO object.
     * @param message
     * @return CustomerCreationDTO
     */
    @Override
    protected CustomerCreationDTO handleMessage(String message) {
        return new Gson().fromJson(message, CustomerCreationDTO.class);
    }
    /**
     * A method that is called once a message has been received. 
     * The method calls the customerAdministrationService.createNewCustomer,
     * and then sends a response over the message queue
     * @param request The Data Transfer Object that contains the necessary information to create the customer
     */
    @Override
    public void receive(CustomerCreationDTO request) {
    	DTUPayResponse<Customer> response=customerAdministrationService.createNewCustomer(request.firstName, request.lastName, request.cpr);
    	ResponseDTO responseDTO=new ResponseDTO();
    	responseDTO.success=response.success();
    	AsyncSender<ResponseDTO> customerCreatedSender
        = new RabbitSender<>(exchange, "administration.customer.created." + request.instanceId);
    	customerCreatedSender.send(responseDTO);
    }
}
