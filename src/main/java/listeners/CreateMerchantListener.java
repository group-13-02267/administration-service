package listeners;

import com.google.gson.Gson;

import administration.MerchantAdministrationService;
import administration.Entities.Merchant;
import core.AsyncSender;
import core.AsyncService;
import core.RabbitSender;
import entities.MerchantCreationDTO;
import entities.MerchantValidationDTO;
import entities.ResponseDTO;
import responses.DTUPayResponse;

/**
 * This class is responsible for listening to messages regarding the merchant creation, 
 * as well as performing the correct action and sending a response.
 * This class extends the functionality of the AsyncService
 * @author Roberts
 *
 */
public class CreateMerchantListener extends AsyncService<MerchantCreationDTO> {

	private final static String exchange = "administration";
	private final MerchantAdministrationService merchantAdministrationService;
	/**
	 * Constructor for the Class
	 * @param merchantAdministrationService A service that will be called once a message has been received
	 */
    public CreateMerchantListener(MerchantAdministrationService merchantAdministrationService) {
        super(exchange, "administration.merchant.create.*");
        this.merchantAdministrationService=merchantAdministrationService;
    }

    /**
     * Handle incoming payload. Converts the payload to a MerchantCreationDTO object.
     * @param message
     * @return MerchantCreationDTO
     */
    @Override
    protected MerchantCreationDTO handleMessage(String message) {
        return new Gson().fromJson(message, MerchantCreationDTO.class);
    }

    /**
     * A method that is called once a message has been received. 
     * The method calls the merchantAdministrationService.createNewCustomer,
     * and then sends a response over the message queue
     * @param request The Data Transfer Object that contains the necessary information to create the merchant
     */
    @Override
    public void receive(MerchantCreationDTO request) {
    	System.out.println("Received a request");
    	DTUPayResponse<Merchant> response=merchantAdministrationService.createMerchant(request.firstName, request.lastName, request.cpr);
    	ResponseDTO responseDTO=new ResponseDTO();
    	responseDTO.success=response.success();
    	AsyncSender<ResponseDTO> merchantCreatedSender
        = new RabbitSender<>(exchange, "administration.merchant.created." + request.instanceId);
    	merchantCreatedSender.send(responseDTO);
    }
}
