package listeners;

import administration.Entities.Merchant;
import administration.MerchantRepository;
import com.google.gson.Gson;
import core.AsyncService;
import entities.MerchantDTO;
import entities.MerchantValidationDTO;
import messaging.MessageSender;

/**
 * This class is responsible for listening to messages regarding the merchant validation, 
 * as well as performing the correct action and sending a response.
 * This class extends the functionality of the AsyncService
 * @author Georg
 *
 */
public class ValidateMerchantListener extends AsyncService<MerchantDTO> {

    private final static String exchange = "payment";
    private final MessageSender sender;
    private final MerchantRepository repository;

    /**
     * The constructor of the class
     * @param repository A MerchantRepository that is used to validate a merchant
     * @param sender A MessageSender used for sending and listening to messages from the message queue
     */
    public ValidateMerchantListener(MerchantRepository repository, MessageSender sender) {
        super(exchange, "validate.merchant");
        this.sender = sender;
        this.repository = repository;
    }

    /**
     * Handle incoming payload. Converts the payload to a MerchantDTO object.
     * @param message
     * @return MerchantDTO
     */
    @Override
    protected MerchantDTO handleMessage(String message) {
        return new Gson().fromJson(message, MerchantDTO.class);
    }

    /**
     * Method that is called once a message has been received from the message queue
     * The method then checks if a merchant is valid by requesting it from the repository
     * Then the response is sent over the message queue
     * @param receive A MerchantDTO object containing the necessary information
     */
    @Override
    public void receive(MerchantDTO receive) {
        Merchant merchant = repository.getMerchantByCpr(receive.merchantId);

        boolean status = merchant != null;

        MerchantValidationDTO merchantValidationDTO = new MerchantValidationDTO(receive.id, status);

        sender.sendMessage(merchantValidationDTO, "merchant.validate." + receive.id, exchange);
    }
}
